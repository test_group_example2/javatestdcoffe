/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject;

import com.mycompany.databaseproject.dao.UserDao;
import com.mycompany.databaseproject.helper.helper;
import com.mycompany.databaseproject.model.User;

/**
 *
 * @author Kitty
 */
public class testDao {

    public static void main(String[] args) {
        UserDao userDao = new UserDao();
        for (User u : userDao.getAll()) {
            System.out.println(u);
        }
        User user1 = userDao.get(1);
        userDao.delete(user1);
        for (User u : userDao.getAll("user_name like 'm%' ","user_name asc , user_gender desc ")) {
            System.out.println(u);
        }
        helper.close();
    }
}

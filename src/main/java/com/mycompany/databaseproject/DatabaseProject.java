/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kitty
 */
public class DatabaseProject {

    public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:Dcoffe_java.db";
        try {

            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite Has been stablish.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }
    }

}
